/*
 * Copyright 2014 (c) by Olliver Schinagl <o.schinagl@ultimaker.com>
 * All rights reserved.
 *
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Affero GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Affero GNU General Public License for more details.
 * 
 * You should have received a copy of the Affero GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#define bit_set(sfr, bit)	((sfr) |= (bit))
#define bit_clear(sfr, bit)	((sfr) &= ~(bit))
#define bit_change(sfr, bit)	((sfr) ^= (bit))

#define FALSE (0)
#define TRUE (!FALSE)
