   1               		.file	"ws2812.c"
   2               	__SP_H__ = 0x3e
   3               	__SP_L__ = 0x3d
   4               	__SREG__ = 0x3f
   5               	__tmp_reg__ = 0
   6               	__zero_reg__ = 1
   7               	 ;  GNU C (GCC) version 4.8.1 (avr)
   8               	 ; 	compiled by GNU C version 4.9.1, GMP version 6.0.0, MPFR version 3.1.2-p3, MPC version 1.0.2
   9               	 ;  GGC heuristics: --param ggc-min-expand=100 --param ggc-min-heapsize=131072
  10               	 ;  options passed:  -imultilib avr25 -D F_CPU=8000000 ws2812.c
  11               	 ;  -mmcu=attiny85 -g -O2 -Wall -std=gnu99 -fverbose-asm
  12               	 ;  options enabled:  -faggressive-loop-optimizations -fauto-inc-dec
  13               	 ;  -fbranch-count-reg -fcaller-saves -fcombine-stack-adjustments -fcommon
  14               	 ;  -fcompare-elim -fcprop-registers -fcrossjumping -fcse-follow-jumps
  15               	 ;  -fdefer-pop -fdevirtualize -fdwarf2-cfi-asm -fearly-inlining
  16               	 ;  -feliminate-unused-debug-types -fexpensive-optimizations
  17               	 ;  -fforward-propagate -ffunction-cse -fgcse -fgcse-lm -fgnu-runtime
  18               	 ;  -fguess-branch-probability -fhoist-adjacent-loads -fident
  19               	 ;  -fif-conversion -fif-conversion2 -findirect-inlining -finline
  20               	 ;  -finline-atomics -finline-functions-called-once
  21               	 ;  -finline-small-functions -fipa-cp -fipa-profile -fipa-pure-const
  22               	 ;  -fipa-reference -fipa-sra -fira-hoist-pressure -fira-share-save-slots
  23               	 ;  -fira-share-spill-slots -fivopts -fkeep-static-consts
  24               	 ;  -fleading-underscore -fmath-errno -fmerge-constants
  25               	 ;  -fmerge-debug-strings -fmove-loop-invariants -fomit-frame-pointer
  26               	 ;  -foptimize-register-move -foptimize-sibling-calls -foptimize-strlen
  27               	 ;  -fpartial-inlining -fpeephole -fpeephole2 -fprefetch-loop-arrays
  28               	 ;  -freg-struct-return -fregmove -freorder-blocks -freorder-functions
  29               	 ;  -frerun-cse-after-loop -fsched-critical-path-heuristic
  30               	 ;  -fsched-dep-count-heuristic -fsched-group-heuristic -fsched-interblock
  31               	 ;  -fsched-last-insn-heuristic -fsched-rank-heuristic -fsched-spec
  32               	 ;  -fsched-spec-insn-heuristic -fsched-stalled-insns-dep -fshow-column
  33               	 ;  -fshrink-wrap -fsigned-zeros -fsplit-ivs-in-unroller -fsplit-wide-types
  34               	 ;  -fstrict-aliasing -fstrict-overflow -fstrict-volatile-bitfields
  35               	 ;  -fsync-libcalls -fthread-jumps -ftoplevel-reorder -ftrapping-math
  36               	 ;  -ftree-bit-ccp -ftree-builtin-call-dce -ftree-ccp -ftree-ch
  37               	 ;  -ftree-coalesce-vars -ftree-copy-prop -ftree-copyrename -ftree-dce
  38               	 ;  -ftree-dominator-opts -ftree-dse -ftree-forwprop -ftree-fre
  39               	 ;  -ftree-loop-if-convert -ftree-loop-im -ftree-loop-ivcanon
  40               	 ;  -ftree-loop-optimize -ftree-parallelize-loops= -ftree-phiprop
  41               	 ;  -ftree-pre -ftree-pta -ftree-reassoc -ftree-scev-cprop -ftree-sink
  42               	 ;  -ftree-slp-vectorize -ftree-slsr -ftree-sra -ftree-switch-conversion
  43               	 ;  -ftree-tail-merge -ftree-ter -ftree-vect-loop-version -ftree-vrp
  44               	 ;  -funit-at-a-time -fverbose-asm -fzero-initialized-in-bss
  45               	
  48               		.text
  49               	.Ltext0:
 146               	.global	output_grb
 148               	output_grb:
 149               		.stabd	46,0,0
   1:ws2812.c      **** /*
   2:ws2812.c      ****  * Copyright 2014 (c) by Olliver Schinagl <oliver@schinagl.nl>
   3:ws2812.c      ****  * All rights reserved.
   4:ws2812.c      ****  *
   5:ws2812.c      ****  *
   6:ws2812.c      ****  * This program is free software: you can redistribute it and/or modify
   7:ws2812.c      ****  * it under the terms of the Affero GNU General Public License as published by
   8:ws2812.c      ****  * the Free Software Foundation, either version 3 of the License, or
   9:ws2812.c      ****  * (at your option) any later version.
  10:ws2812.c      ****  *
  11:ws2812.c      ****  * This program is distributed in the hope that it will be useful,
  12:ws2812.c      ****  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  13:ws2812.c      ****  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  14:ws2812.c      ****  * Affero GNU General Public License for more details.
  15:ws2812.c      ****  * 
  16:ws2812.c      ****  * You should have received a copy of the Affero GNU General Public License
  17:ws2812.c      ****  * along with this program.  If not, see <http://www.gnu.org/licenses/>.
  18:ws2812.c      ****  * 
  19:ws2812.c      ****  */
  20:ws2812.c      **** 
  21:ws2812.c      **** #define __SFR_OFFSET 0 
  22:ws2812.c      **** #include <avr/interrupt.h>
  23:ws2812.c      **** #include <avr/io.h>
  24:ws2812.c      **** #include <avr/sfr_defs.h>
  25:ws2812.c      **** #include <stdint.h>
  26:ws2812.c      **** 
  27:ws2812.c      **** #include "bitops.h"
  28:ws2812.c      **** 
  29:ws2812.c      **** 
  30:ws2812.c      ****  
  31:ws2812.c      **** /*
  32:ws2812.c      **** ;extern void output_grb(u8 * ptr, u16 count)
  33:ws2812.c      **** ;
  34:ws2812.c      **** ; r18 = data byte
  35:ws2812.c      **** ; r19 = 7-bit count
  36:ws2812.c      **** ; r20 = 1 output
  37:ws2812.c      **** ; r21 = 0 output
  38:ws2812.c      **** ; r22 = SREG save
  39:ws2812.c      **** ; r24:25 = 16-bit count
  40:ws2812.c      **** ; r26:27 (X) = data pointer
  41:ws2812.c      ****  
  42:ws2812.c      **** .equ      OUTBIT,   0
  43:ws2812.c      ****  
  44:ws2812.c      ****  
  45:ws2812.c      **** .global output_grb
  46:ws2812.c      **** output_grb:
  47:ws2812.c      **** */
  48:ws2812.c      **** void output_grb(uint8_t *p_cmd, uint16_t count) {
 151               	.LM0:
 152               	.LFBB1:
 153               	/* prologue: function */
 154               	/* frame size = 0 */
 155               	/* stack size = 0 */
 156               	.L__stack_usage = 0
  49:ws2812.c      **** 	__asm__("									\
 158               	.LM1:
 159               	/* #APP */
 160               	 ;  49 "ws2812.c" 1
 161 0000 DC01      													 movw   r26, r24      ;r26:27 = X = p_cmd						 movw   r24, r22      ;r24:25 = count			
 162               	 ;  0 "" 2
 163               	/* #NOAPP */
 164 0002 0895      		ret
 166               	.Lscope1:
 168               		.stabd	78,0,0
 169               		.section	.text.startup,"ax",@progbits
 171               	.global	main
 173               	main:
 174               		.stabd	46,0,0
  50:ws2812.c      **** 		 movw   r26, r24      ;r26:27 = X = p_cmd				\
  51:ws2812.c      **** 		 movw   r24, r22      ;r24:25 = count					\
  52:ws2812.c      **** 		 in     r22, SREG     ;save SREG (global int state)			\
  53:ws2812.c      **** 		 cli                  ;no interrupts from here on, we're cycle-counting	\
  54:ws2812.c      **** 		 in     r20, PORTB							\
  55:ws2812.c      **** 		 ori    r20, (1<<OUTBIT)         ;our '1' output			\
  56:ws2812.c      **** 		 in     r21, PORTB							\
  57:ws2812.c      **** 		 andi   r21, ~(1<<OUTBIT)        ;our '0' output			\
  58:ws2812.c      **** 		 ldi    r19, 7        ; 7 bit counter (8th bit is different)		\
  59:ws2812.c      **** 		 ld     r18, X+       ; get first data byte				\
  60:ws2812.c      **** 	loop1:			      ; cost count					\
  61:ws2812.c      **** 		 out    PORTB, r20    ; 1   +0 start of a bit pulse			\
  62:ws2812.c      **** 		 lsl    r18           ; 1   +1 next bit into C, MSB first		\
  63:ws2812.c      **** 		 brcs   L1            ; 1/2 +2 branch if 1				\
  64:ws2812.c      **** 		 out    PORTB, r21    ; 1   +3 end hi for '0' bit (3 clocks hi)		\
  65:ws2812.c      **** 		 nop                  ; 1   +4						\
  66:ws2812.c      **** 		 bst    r18, 7        ; 1   +5 save last bit of data for fast branching	\
  67:ws2812.c      **** 		 subi   r19, 1        ; 1   +6 how many more bits for this byte?	\
  68:ws2812.c      **** 		 breq   bit8          ; 1/2 +7 last bit, do differently			\
  69:ws2812.c      **** 		 rjmp   loop1         ; 2   +8, 10 total for 0 bit			\
  70:ws2812.c      **** 	L1:										\
  71:ws2812.c      **** 		 nop                  ; 1   +4						\
  72:ws2812.c      **** 		 bst    r18, 7        ; 1   +5 save last bit of data for fast branching	\
  73:ws2812.c      **** 		 subi   r19, 1        ; 1   +6 how many more bits for this byte		\
  74:ws2812.c      **** 		 out    PORTB, r21    ; 1   +7 end hi for '1' bit (7 clocks hi)		\
  75:ws2812.c      **** 		 brne   loop1         ; 2/1 +8 10 tot for 1 bit (fall thru if lst bit)	\
  76:ws2812.c      **** 	bit8:										\
  77:ws2812.c      **** 		 ldi    r19, 7        ; 1   +9 bit count for next byte			\
  78:ws2812.c      **** 		 out    PORTB, r20    ; 1   +0 start of a bit pulse			\
  79:ws2812.c      **** 		 brts   L2            ; 1/2 +1 branch if last bit is a 1		\
  80:ws2812.c      **** 		 nop                  ; 1   +2						\
  81:ws2812.c      **** 		 out    PORTB, r21    ; 1   +3 end hi for '0' bit (3 clocks hi)		\
  82:ws2812.c      **** 		 ld     r18, X+       ; 2   +4 fetch next byte				\
  83:ws2812.c      **** 		 sbiw   r24, 1        ; 2   +6 dec byte counter				\
  84:ws2812.c      **** 		 brne   loop1         ; 2   +8 loop back or return			\
  85:ws2812.c      **** 		 out    SREG, r22     ; 1   +0 restore global int flag			\
  86:ws2812.c      **** 		 ret									\
  87:ws2812.c      **** 	L2:										\
  88:ws2812.c      **** 		 ld     r18, X+       ; 2   +3 fetch next byte				\
  89:ws2812.c      **** 		 sbiw   r24, 1        ; 2   +5 dec byte counter				\
  90:ws2812.c      **** 		 out     PORTB, r21   ; 1   +7 end hi for '1' bit (7 clocks hi)		\
  91:ws2812.c      **** 		 brne   loop1         ; 2   +8 loop back or return			\
  92:ws2812.c      **** 		 out    SREG, r22     ; 1   +9 restore global int flag			\
  93:ws2812.c      **** 		 ret									\
  94:ws2812.c      **** 	 " /* no input */ : /* no output */ : /* no changes */);
  95:ws2812.c      **** };
  96:ws2812.c      **** 
  97:ws2812.c      **** int main(void) {
 176               	.LM2:
 177               	.LFBB2:
 178               	/* prologue: function */
 179               	/* frame size = 0 */
 180               	/* stack size = 0 */
 181               	.L__stack_usage = 0
 182               	.L3:
 183               	.LBB4:
 184               	.LBB5:
  49:ws2812.c      **** 	__asm__("									\
 186               	.LM3:
 187               	/* #APP */
 188               	 ;  49 "ws2812.c" 1
 189 0000 DC01      													 movw   r26, r24      ;r26:27 = X = p_cmd						 movw   r24, r22      ;r24:25 = count			
 190               	 ;  0 "" 2
 191               	/* #NOAPP */
 192 0002 00C0      		rjmp .L3	 ; 
 193               	.LBE5:
 194               	.LBE4:
 196               	.Lscope2:
 198               		.stabd	78,0,0
 199               		.text
 201               	.Letext0:
 202               		.ident	"GCC: (GNU) 4.8.1"
